package com.example.torressilvarichard.vista;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.torressilvarichard.Modelo.Artista;
import com.example.torressilvarichard.R;
import com.example.torressilvarichard.vista.adapter.artistaAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


public class ActivityMemoriaApp extends AppCompatActivity implements View.OnClickListener {

    Button leer;
    TextView mostrar;
    RecyclerView recyclerViewArtista;
    artistaAdapter adapter;
    List<Artista> lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memoria_app);
        tomar();
    }

    public void tomar (){
        leer = findViewById(R.id.btn_leer);
        mostrar = findViewById(R.id.lbl_lectura);
        recyclerViewArtista = findViewById(R.id.listaRe);
        leer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        try {
            InputStream input = getResources().openRawResource(R.raw.archivo_raw);
            BufferedReader lector = new BufferedReader(new InputStreamReader(input));
            String lineas = lector.readLine();
            String[] xd = lineas.split(";");
            mostrar.setText(lineas);
            for (int i = 0; i < xd.length; i++)
            {
                String[] split = xd[i].split(",");
                Artista a = new Artista();
                a.setNombres(split[0]);
                a.setApellidos(split[1]);
                a.setPseudo(split[2]);
                lista.add(a);
            }
            adapter = new artistaAdapter(lista);
            recyclerViewArtista.setLayoutManager(new LinearLayoutManager(this));
            recyclerViewArtista.setAdapter(adapter);
        }catch (Exception ex){
            Log.e("Archivo_RAW", "error de Lectura" + ex.getMessage());
        }
    }

}
