package com.example.torressilvarichard.vista.adapter;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.torressilvarichard.Modelo.Artista;
import com.example.torressilvarichard.R;

import java.util.List;

public class artistaAdapter extends RecyclerView.Adapter<artistaAdapter.ViewHolderArtista>{

        List<Artista> lista;
    private ProductoIn recyclerViewOnItemClickListener;

        public artistaAdapter(List<Artista> lista){
            this.lista = lista;
        }

    public artistaAdapter(List<Artista> lista, ProductoIn recyclerViewOnItemClickListener){
        this.lista = lista;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }

    @Override
    public ViewHolderArtista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artista,parent,false);
        return new ViewHolderArtista(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderArtista holder, int position) {
        holder.dato1.setText(lista.get(position).getNombres());
        holder.dato2.setText(lista.get(position).getApellidos());
        holder.dato3.setText(lista.get(position).getPseudo());
        if (lista.get(position).getFoto()!= null) {
            Uri url = Uri.parse(lista.get(position).getFoto());
            Log.e("URL", url.toString());
            holder.imagen.setImageURI(url);
        }
    }

        @Override
        public int getItemCount() {
            return lista.size();
        }

    public  class ViewHolderArtista extends RecyclerView.ViewHolder implements View.OnClickListener
    {   TextView dato1,dato3;
        TextView dato2;
        ImageView imagen;
        public ViewHolderArtista(@NonNull View itemView) {
            super(itemView);
            dato1 = itemView.findViewById(R.id.lbl_nombre_artista);
            dato2 = itemView.findViewById(R.id.lbl_apellido_artista);
            imagen= itemView.findViewById(R.id.imagen);
            dato3 = itemView.findViewById(R.id.lbl_nombre_comercial_artista);
            itemView.setOnClickListener(this);

        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onCLick(v,getAdapterPosition());
        }


    }


}
