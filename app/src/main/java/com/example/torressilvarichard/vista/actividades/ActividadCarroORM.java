package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torressilvarichard.Modelo.Vehiculo;
import com.example.torressilvarichard.R;
import com.example.torressilvarichard.vista.adapter.ProductoIn;
import com.example.torressilvarichard.vista.adapter.vehiculoAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadCarroORM extends AppCompatActivity implements View.OnClickListener {

    EditText placa, marca, modelo, year;
    Button agregar, eliminar, modificar, buscarCarro, buscarPlaca, eliminarPlaca;
    TextView datos;
    RecyclerView listarCarro;

    List<Vehiculo> carros;
    vehiculoAdapter vA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_carro_orm);

        control();
    }

    public void control(){
        placa = findViewById(R.id.txtPlaca);
        marca = findViewById(R.id.txtMarca);
        modelo = findViewById(R.id.txtModelo);
        year = findViewById(R.id.txtYear);

        datos = findViewById(R.id.lblDatos);

        agregar = findViewById(R.id.btnIngresar);
        eliminar = findViewById(R.id.btnEliminarCarro);
        modificar = findViewById(R.id.btnModificar);
        eliminarPlaca = findViewById(R.id.btnEliminarPlaca);
        buscarCarro = findViewById(R.id.btnBuscarCarro);
        buscarPlaca = findViewById(R.id.btnBuscarPlaca);

        listarCarro = findViewById(R.id.ListarCarros);

        agregar.setOnClickListener(this);
        eliminar.setOnClickListener(this);
        modificar.setOnClickListener(this);
        buscarCarro.setOnClickListener(this);
        buscarPlaca.setOnClickListener(this);
        eliminarPlaca.setOnClickListener(this);
    }

    Vehiculo aux;

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnIngresar:
                aux = new Vehiculo();
                aux.setMlaca(placa.getText().toString());
                aux.setMarca(marca.getText().toString());
                aux.setModelo(modelo.getText().toString());
                aux.setYear(Integer.parseInt(year.getText().toString()));
                aux.save();
                Toast.makeText(view.getContext(), "vehiculo guardado con éxito", Toast.LENGTH_SHORT).show();
                // save para modificar
                break;

            case R.id.btnBuscarCarro:
                datos.setText("numero de carros " + Vehiculo.getAllCarro().size());
                carros = Vehiculo.getAllCarro();
                cargarRecycler(carros);
                break;
            case  R.id.btnEliminarCarro:
                Vehiculo.deleteAll();
                carros = Vehiculo.getAllCarro();
                cargarRecycler(carros);
                break;
            case R.id.btnEliminarPlaca:
                Vehiculo.eliminarPlaca(placa.getText().toString());
                carros = Vehiculo.getAllCarro();
                break;
            case R.id.btnBuscarPlaca:
                carros.clear();
                carros.add(Vehiculo.getCarroPlate(placa.getText().toString()));
                cargarRecycler(carros);
                break;
            case R.id.btnModificar:
                aux = new Vehiculo();
                aux.setMlaca(placa.getText().toString());
                aux.setMarca(marca.getText().toString());
                aux.setModelo(modelo.getText().toString());
                aux.setYear(Integer.parseInt(year.getText().toString()));
                Vehiculo.modificar(aux);
                carros = Vehiculo.getAllCarro();
                Toast.makeText(this,"Carro modificado", Toast.LENGTH_LONG).show();
                break;

        }
    }

    public void cargarRecycler(List<Vehiculo> lista){

        vA = new vehiculoAdapter(lista, new ProductoIn() {
            @Override
            public void onCLick(View v, int pos) {
                TextView lMarca, lmodelo, lplaca, lyear;
                lMarca = v.findViewById(R.id.lblMarca);
                lmodelo = v.findViewById(R.id.lblModelo);
                lplaca = v.findViewById(R.id.lblPlaca);
                lyear = v.findViewById(R.id.lblyear);

                placa.setText(lplaca.getText().toString());
                marca.setText(lMarca.getText().toString());
                modelo.setText(lmodelo.getText().toString());
                year.setText(lyear.getText().toString());

            }
        });

        listarCarro.setLayoutManager(new LinearLayoutManager(this));

        listarCarro.setAdapter(vA);

    }
}
