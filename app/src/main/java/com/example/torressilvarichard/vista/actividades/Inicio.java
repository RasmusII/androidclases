package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.torressilvarichard.R;

public class Inicio extends AppCompatActivity implements View.OnClickListener{

    Button suma, log, paramet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        cargarComponentes();

    }

    private void cargarComponentes(){
        suma = findViewById(R.id.btnSuma);
        log = findViewById(R.id.btnLog);
        paramet = findViewById(R.id.btnParametros);
        suma.setOnClickListener(this);
        log.setOnClickListener(this);
        paramet.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()){
            case R.id.btnLog:
                intent = new Intent(Inicio.this, activity_login.class);
                startActivity(intent);
                break;
            case R.id.btnSuma:
                intent = new Intent(Inicio.this, activity_suma.class);
                startActivity(intent);
                break;
            case R.id.btnParametros:
                intent = new Intent(Inicio.this, ActivityParametro.class);
                startActivity(intent);
                break;
        }
    }
}
