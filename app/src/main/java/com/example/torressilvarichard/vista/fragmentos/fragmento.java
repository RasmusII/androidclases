package com.example.torressilvarichard.vista.fragmentos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.torressilvarichard.R;

public class fragmento extends AppCompatActivity implements View.OnClickListener, frg1.OnFragmentInteractionListener, frg2.OnFragmentInteractionListener {

    Button botonFrg1, botonFrg2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmento);

        cargarComponentes();
    }

    public void cargarComponentes(){
        botonFrg1 = findViewById(R.id.btnFrg1);
        botonFrg2 = findViewById(R.id.btnFrg2);
        botonFrg1.setOnClickListener(this);
        botonFrg2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrg1 :
                frg1 fragmento1 = new frg1();
                FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
                transaccion1.replace(R.id.contenedor,fragmento1);
                transaccion1.commit();
                break;
            case R.id.btnFrg2:
                frg2 fragmento2 = new frg2();
                FragmentTransaction transaccion2 = getSupportFragmentManager().beginTransaction();
                transaccion2.replace(R.id.contenedor,fragmento2);
                transaccion2.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
