package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.torressilvarichard.R;

public class ActivityParametro extends AppCompatActivity implements View.OnClickListener{

    EditText name, lastName;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametro);

        cargarComponentes();

    }

    public void cargarComponentes(){
        name = findViewById(R.id.txtNombreEnviarParametro);
        lastName = findViewById(R.id.txtApellidoEnviarParametro);
        enviar = findViewById(R.id.btnEnviarParametro);

        enviar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(ActivityParametro.this, ActivityRecibirParametros.class);
        intent.putExtra("caja_nombre", name.getText().toString());
        intent.putExtra("apellido", lastName.getText().toString());
        startActivity(intent);
    }
}
