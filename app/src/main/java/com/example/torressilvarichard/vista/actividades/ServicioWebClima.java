package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torressilvarichard.R;
import com.example.torressilvarichard.controlador.AsyncInterface;
import com.example.torressilvarichard.controlador.ServicioClima;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class ServicioWebClima extends AppCompatActivity implements AsyncInterface {

    TextView longitud, latitud, idw, main, desc, icon, base, temp, pressure, humidity, temp_min, temp_max, visibility, speed, deg, all, dt, type, ids, message, country, sunrise, sunset, idt, name, cod;

    ServicioClima servicioClima;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_web_clima);

        control();
        servicioClima.execute("https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22","1");
    }

    public void control (){
        longitud = findViewById(R.id.lbl_Longitud);
        latitud = findViewById(R.id.lbl_Latitud);
        main = findViewById(R.id.lbl_Main);
        desc = findViewById(R.id.lbl_descripcion);
        icon = findViewById(R.id.lbl_Icon);
        base = findViewById(R.id.lbl_Base);
        temp = findViewById(R.id.lbl_temp);
        pressure = findViewById(R.id.lbl_Presion);
        humidity = findViewById(R.id.lbl_humedad);
        temp_min = findViewById(R.id.lbl_TempMin);
        temp_max = findViewById(R.id.lbl_TempMax);
        visibility = findViewById(R.id.lbl_Visibilidad);
        speed = findViewById(R.id.lbl_Speed);
        deg = findViewById(R.id.lbl_Grados);
        all = findViewById(R.id.lbl_All);
        dt = findViewById(R.id.lbl_Dt);
        type = findViewById(R.id.lbl_type);
        message = findViewById(R.id.lbl_message);
        country = findViewById(R.id.lbl_Country);
        sunrise = findViewById(R.id.lbl_sunrise);
        sunset = findViewById(R.id.lbl_sunset);
        name = findViewById(R.id.lbl_name); 
        cod = findViewById(R.id.lbl_Codigo);

    }


    @Override
    public void respuesta(HashMap<String, Object> response) {
        latitud.setText(response.get("lat").toString());
        longitud.setText(response.get("lon").toString());
        icon.setText(response.get("icon").toString());
        main.setText(response.get("weather_main").toString());
        desc.setText(response.get("weather_description").toString());
        name.setText(response.get("name").toString());
        base.setText(response.get("base").toString());
        temp.setText(response.get("temp").toString());
        temp_max.setText(response.get("temp_max").toString());
        temp_min.setText(response.get("temp_min").toString());
        humidity.setText(response.get("humidity").toString());
        pressure.setText(response.get("pressure").toString());
        deg.setText(response.get("deg").toString());
        speed.setText(response.get("speed").toString());
        message.setText(response.get("message").toString());
        country.setText(response.get("country").toString());
        sunrise.setText(response.get("sunrise").toString());
        sunset.setText(response.get("sunset").toString());
        dt.setText(response.get("dt").toString());
        type.setText(response.get("type").toString());
        all.setText(response.get("all").toString());
        visibility.setText(response.get("visibility").toString());
        cod.setText(response.get("code").toString());
    }
}
