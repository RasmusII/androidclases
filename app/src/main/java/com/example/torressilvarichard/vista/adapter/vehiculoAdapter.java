package com.example.torressilvarichard.vista.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
;
import androidx.recyclerview.widget.RecyclerView;

import com.example.torressilvarichard.Modelo.Vehiculo;
import com.example.torressilvarichard.R;

import java.util.List;

public class vehiculoAdapter extends RecyclerView.Adapter<vehiculoAdapter.ViewHolderVehiculo> {

    List<Vehiculo> carros;
    private ProductoIn ListaCarros;

    public vehiculoAdapter(List<Vehiculo> carros){
        this.carros = carros;
    }

    public vehiculoAdapter(List<Vehiculo> carros, ProductoIn ListaCarros){
        this.carros = carros;
        this.ListaCarros = ListaCarros;
    }

    @Override
    public ViewHolderVehiculo onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vehiculo,parent,false);
        return new ViewHolderVehiculo(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderVehiculo holder, int position) {
        holder.placa.setText(carros.get(position).getPlaca());
        holder.modelo.setText(carros.get(position).getModelo());
        holder.marca.setText(carros.get(position).getMarca());
        holder.year.setText(String.valueOf(carros.get(position).getYear()   ));
    }

    @Override
    public int getItemCount() {
        return carros.size();
    }

    public class ViewHolderVehiculo extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView placa, marca, modelo, year;

        public ViewHolderVehiculo(@NonNull View itemView){
            super(itemView);
            placa = itemView.findViewById(R.id.lblPlaca);
            marca = itemView.findViewById(R.id.lblMarca);
            modelo = itemView.findViewById(R.id.lblModelo);
            year = itemView.findViewById(R.id.lblyear);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            ListaCarros.onCLick(view, getAdapterPosition());
        }
    }
}
