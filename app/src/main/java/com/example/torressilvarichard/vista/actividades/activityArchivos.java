package com.example.torressilvarichard.vista.actividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.torressilvarichard.R;
import com.example.torressilvarichard.vista.fragmentos.Externa;
import com.example.torressilvarichard.vista.fragmentos.Leer_interno;
import com.example.torressilvarichard.vista.fragmentos.memoriaInterna;

public class activityArchivos extends AppCompatActivity implements Externa.OnFragmentInteractionListener, Leer_interno.OnFragmentInteractionListener, memoriaInterna.OnFragmentInteractionListener {

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archivos);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        FrameLayout fr = findViewById(R.id.fragmento_artistas);
        fr.removeAllViews();
        switch (item.getItemId()){

            case R.id.opcion_interno:
                memoriaInterna fragmento2 = new memoriaInterna();
                FragmentTransaction transaccion2 = getSupportFragmentManager().beginTransaction();
                transaccion2.replace(R.id.fragmento_artistas,fragmento2);
                transaccion2.commit();
                break;
            case R.id.opcion_lerr_interno:
                Leer_interno fragmento1 = new Leer_interno();
                FragmentTransaction transaccion1 = getSupportFragmentManager().beginTransaction();
                transaccion1.replace(R.id.fragmento_artistas,fragmento1);
                transaccion1.commit();
                break;
            case R.id.opcion_EXTERNA:
                Externa fragmento3 = new Externa();
                FragmentTransaction transaccion3 = getSupportFragmentManager().beginTransaction();
                transaccion3.replace(R.id.fragmento_artistas,fragmento3);
                transaccion3.commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
