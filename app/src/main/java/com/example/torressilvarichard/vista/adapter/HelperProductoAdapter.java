package com.example.torressilvarichard.vista.adapter;


import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.torressilvarichard.Modelo.Producto;
import com.example.torressilvarichard.R;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class HelperProductoAdapter extends RecyclerView.Adapter<HelperProductoAdapter.ViewHolderProducto> {

    List<Producto> lista;

    private static ProductoIn inter;

    public HelperProductoAdapter(List<Producto> lista, ProductoIn inter){
        this.lista = lista;
        this.inter = inter;
    }

    public HelperProductoAdapter(List<Producto> lista){
        this.lista = lista;
    }


    @Override
    public ViewHolderProducto onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, null);

        return new ViewHolderProducto(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderProducto holder, int position) {

        holder.codigo.setText(String.valueOf(lista.get(position).getCodigo()));
        holder.descripcion.setText(lista.get(position).getDescripcion());
        holder.cantidad.setText(String.valueOf(lista.get(position).getCantidad()));
        holder.precio.setText(String.valueOf(lista.get(position).getPrecio()));

    }

        @Override
        public int getItemCount() {
            return lista.size();
        }

        public static class ViewHolderProducto extends RecyclerView.ViewHolder implements View.OnClickListener{

            TextView codigo, descripcion, precio, cantidad;

            public ViewHolderProducto(View view){
                super(view);
                codigo = view.findViewById(R.id.lblCodigo);
                descripcion = view.findViewById(R.id.lblDescripsion);
                precio = view.findViewById(R.id.lblPrecio);
                cantidad = view.findViewById(R.id.lblCantidadPro);

                itemView.setOnClickListener(this);
            }


            @Override
            public void onClick(View view) {
                inter.onCLick(view, getAdapterPosition());
            }
        }


}
