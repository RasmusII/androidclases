package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.torressilvarichard.R;

public class activity_suma extends AppCompatActivity implements View.OnClickListener{

    EditText valor1, valor2;
    Button sumar;
    TextView resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suma);
        cargarComponentes();

    }

    private void cargarComponentes(){
        valor1 = findViewById(R.id.txtValor1);
        valor2 = findViewById(R.id.txtValor2);
        sumar = findViewById(R.id.btnSumar);
        resultado = findViewById(R.id.lblResultado);
        sumar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Float s;
        s = Float.parseFloat(valor1.getText().toString()) + Float.parseFloat(valor2.getText().toString());
        resultado.setText("SUMA: " + s.toString());
    }
}
