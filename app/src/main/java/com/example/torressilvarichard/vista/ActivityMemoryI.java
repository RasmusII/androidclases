package com.example.torressilvarichard.vista;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.torressilvarichard.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class ActivityMemoryI extends AppCompatActivity implements View.OnClickListener{

    Button botonGuardar, botonBuscarTodos, botonFoto;
    EditText cajaNombres, cajaApellidos, cajaArtistico;
    TextView datos;
    ImageView foto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_i);
        tomarControl();
    }

    public void tomarControl(){
        botonGuardar = findViewById(R.id.btn_guardarMI);
        botonBuscarTodos = findViewById(R.id.btnBuscarMI);
        botonFoto = findViewById(R.id.btn_foto);
        cajaNombres = findViewById(R.id.txt_Nombres);
        cajaApellidos = findViewById(R.id.txt_Apellidos);
        cajaArtistico = findViewById(R.id.txt_nombreArtistico);
        datos = findViewById(R.id.lbl_verificar);
        botonBuscarTodos.setOnClickListener(this);
        botonGuardar.setOnClickListener(this);
        botonFoto.setOnClickListener(this);
        foto = findViewById(R.id.imgFoto);
    }

    private final int SELECT_PICTURE = 200;
    private final int PHOTO_CODE = 100;

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_guardarMI:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaApellidos.getText().toString() + ", " + cajaNombres.getText().toString() +";");
                    escritor.close();

                }catch (Exception ex) {
                    Log.e("ArchivoMI", "error de escritura" + ex.getMessage());
                }

                break;
            case R.id.btnBuscarMI:
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String lineas = lector.readLine();
                    lector.close();
                    datos.setText(lineas);
                }catch (Exception ex){
                    Log.e("ArchivoMI", "error de Lectura" + ex.getMessage());
                }
                break;
            case R.id.btn_foto:
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Selecciona una Imágen"), SELECT_PICTURE);
                }catch (Exception ex){
                    Log.e("Imagen", "error de Abrir imagenes" + ex.getMessage());
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int RequestCode, int ResultCode, Intent data){
        super.onActivityResult(RequestCode, ResultCode, data);

            Uri im = data.getData();
            foto.setImageURI(im);

            datos.setText(im.toString());

    }

}
