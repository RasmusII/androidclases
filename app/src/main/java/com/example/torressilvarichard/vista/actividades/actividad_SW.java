package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torressilvarichard.Modelo.alumno;
import com.example.torressilvarichard.R;
import com.example.torressilvarichard.vista.adapter.ProductoIn;
import com.example.torressilvarichard.vista.adapter.alumnoAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class actividad_SW extends AppCompatActivity implements View.OnClickListener {

    EditText caja_id, caja_nombre, caja_direccion;
    Button agregar, buscar, buscarID, modificar, eliminar, eliminarID;
    TextView datos;
    RecyclerView Recycler;

    alumnoAdapter adapter;
    List<alumno> lista;


    //Declaración de los componentes del SW
    String host = "http://reneguaman.000webhostapp.com";
    String insert = "/insertar_alumno.php";
    String get = "/obtener_alumnos.php";
    String getID = "/obtener_alumno_por_id.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";

    ServicioWeb sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad__sw);

        controlar();
    }

    //acceder al SW mediante Hilo

    class ServicioWeb extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... parametros){

            String consulta = "";
            URL url = null;
            String ruta = parametros[0];
            if(parametros[1].equals("1")){
                try {
                    url = new URL(ruta);
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    int codigoRespuesta = conexion.getResponseCode();
                    if(codigoRespuesta == HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                        consulta += lector.readLine();
                        //Log.e("mensaje", consulta);
                    }
                    else {
                        Log.e("ERROR CODE", "NO VALE EL CODIGO " + codigoRespuesta);
                    }

                    ((HttpURLConnection) conexion).disconnect();

                }catch (Exception ex){
                    ex.printStackTrace();
                    Log.e("Error","MALARDO BRO");
                }
            }else if(parametros[1].equals("2")){
                try {
                    url = new URL(ruta);
                    URLConnection conexion = (URLConnection) url.openConnection();

                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);

                    conexion.connect();

                    Log.e("mensaje", conexion.toString());

                    JSONObject json = new JSONObject();
                    json.put("nombre", parametros[2]);
                    json.put("direccion", parametros[3]);

                    Log.e("JSON",json.getString("nombre"));

                    // realizanco la conexion con el SW
                    OutputStream os = conexion.getOutputStream();
                    Log.e("error", os.toString());
                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));


                    bw.write(json.toString());
                    bw.flush();
                    bw.close();
                    Log.e("error 2 ", ((HttpURLConnection)conexion).getResponseCode()+"");
                    ((HttpURLConnection) conexion).disconnect();
                }catch (Exception ex){
                    Log.e("error", ex.getMessage(), ex.getCause());
                }
            } else if(parametros[1].equals("3")){
                try {
                    url = new URL(ruta);

                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();

                    if (conexion.getResponseCode() == HttpURLConnection.HTTP_OK){
                        InputStream is = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        consulta += br.readLine();
                    }
                    ((HttpURLConnection) conexion).disconnect();

                }catch (Exception ex){
                    Log.e("error", ex.getMessage(), ex.getCause());
                }
            } else if(parametros[1].equals("4")){
                try {
                    url = new URL(ruta);

                    URLConnection conexion = (URLConnection) url.openConnection();

                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);
                    conexion.connect();

                    JSONObject json = new JSONObject();
                    json.put("idalumno", parametros[2]);

                    OutputStream os = conexion.getOutputStream();

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    bw.write(json.toString());
                    bw.flush();
                    bw.close();

                    if(((HttpURLConnection)conexion).getResponseCode() == HttpURLConnection.HTTP_OK){
                        InputStream is = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        consulta += br.readLine();
                    }

                    Log.e("error 2 ", ((HttpURLConnection)conexion).getResponseCode()+"");
                    ((HttpURLConnection) conexion).disconnect();
                }catch (Exception ex){
                    Log.e("error", ex.getMessage(), ex.getCause());
                }
            }else if (parametros[1].equals("5")){
                try {
                    url = new URL(ruta);
                    URLConnection conexion = (URLConnection) url.openConnection();

                    conexion.setDoInput(true);
                    conexion.setDoOutput(true);
                    conexion.setUseCaches(false);
                    conexion.connect();

                    JSONObject json = new JSONObject();
                    json.put("nombre", parametros[2]);
                    json.put("direccion", parametros[3]);
                    json.put("idalumno", parametros[4]);

                    OutputStream os = conexion.getOutputStream();
                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    bw.write(json.toString());
                    bw.flush();
                    bw.close();


                    if(((HttpURLConnection)conexion).getResponseCode() == HttpURLConnection.HTTP_OK){
                        InputStream is = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        consulta += br.readLine();
                    }

                    ((HttpURLConnection) conexion).disconnect();

                }catch (Exception ex){
                    Log.e("error", ex.getMessage(), ex.getCause());
                }
            }

            return consulta;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != ""){
                Lectura(s);
            }
        }
    }

    public void controlar(){
        caja_id = findViewById(R.id.txt_IdSw);
        caja_nombre = findViewById(R.id.txt_NombreSW);
        caja_direccion = findViewById(R.id.txt_direccionSW);

        agregar =findViewById(R.id.btnAgregarSW);
        buscar = findViewById(R.id.btnBuscarSW);
        buscarID = findViewById(R.id.btnBuscarIdSW);
        modificar = findViewById(R.id.btnModificarSW);
        eliminar = findViewById(R.id.btnEliminarAllSW);
        eliminarID = findViewById(R.id.btnEliminarIdSW);

        agregar.setOnClickListener(this);
        buscar.setOnClickListener(this);
        buscarID.setOnClickListener(this);
        modificar.setOnClickListener(this);
        eliminar.setOnClickListener(this);
        eliminarID.setOnClickListener(this);

        datos = findViewById(R.id.lbl_DatosSw);

        Recycler = findViewById(R.id.RecyclerSW);

    }

    @Override
    public void onClick(View view) {

        sw = new ServicioWeb();
        switch (view.getId()){
            case R.id.btnAgregarSW:
                sw.execute(host.concat(insert), "2", caja_nombre.getText().toString(), caja_direccion.getText().toString());
                break;
            case R.id.btnBuscarSW:
                sw.execute(host.concat(get), "1"); // ejecuta el Hilo en el doInBackground
                break;
            case R.id.btnBuscarIdSW:
                sw.execute(host.concat(getID).concat("?idalumno="+caja_id.getText().toString()), "3");
                break;
            case R.id.btnEliminarIdSW:
                sw.execute(host.concat(delete),"4", caja_id.getText().toString());
                break;
            case R.id.btnModificarSW:
                sw.execute(host.concat(update),"5",caja_nombre.getText().toString(), caja_direccion.getText().toString(), caja_id.getText().toString());
                break;
        }
    }


    public void Lectura(String datos){

        try {
            lista = new ArrayList<>();
            JSONObject object = new JSONObject(datos);
            if (object.has("alumno")){
                JSONObject alumno = object.getJSONObject("alumno");
                alumno a = new alumno();
                a.setId(alumno.getInt("idAlumno"));
                a.setDireccion(alumno.getString("caja_direccion"));
                a.setNombre(alumno.getString("caja_nombre"));
                lista.add(a);
            }
            else if(object.has("alumnos")){
                JSONArray arr = object.optJSONArray("alumnos");
                //  Log.e("arreglo",arr.getString(0));
                for (int i = 0; i <arr.length() ; i++) {
                    alumno a = new alumno();
                    a.setId(arr.getJSONObject(i).getInt("idalumno"));
                    a.setDireccion(arr.getJSONObject(i).getString("direccion"));
                    a.setNombre(arr.getJSONObject(i).getString("nombre"));
                    lista.add(a);

                }
            }else
            {
                Toast.makeText(getApplicationContext(),object.getString("mensaje"),Toast.LENGTH_LONG).show();
            }
            cargaRecycler();
        }catch (Exception e){
            e.printStackTrace();
            Log.e("error",e.toString());
        }
    }

    public void cargaRecycler(){
        adapter = new alumnoAdapter(lista, new ProductoIn() {
            @Override
            public void onCLick(View v, int pos) {
                TextView nombre =  v.findViewById(R.id.txt_recycler_nombrealumno);
                TextView direccion = v.findViewById(R.id.txt_direccionalumno);
                TextView id = v.findViewById(R.id.txt_recycler_alumnoid);

                caja_nombre.setText(nombre.getText().toString());
                caja_direccion.setText(direccion.getText().toString());
                caja_id.setText(id.getText().toString());
            }
        });

        Recycler.setLayoutManager(new LinearLayoutManager(this));
        Recycler.setAdapter(adapter);

    }
}
