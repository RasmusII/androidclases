package com.example.torressilvarichard.vista.fragmentos;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.torressilvarichard.Modelo.Artista;
import com.example.torressilvarichard.R;
import com.example.torressilvarichard.vista.adapter.ProductoIn;
import com.example.torressilvarichard.vista.adapter.artistaAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link memoriaInterna.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link memoriaInterna#newInstance} factory method to
 * create an instance of this fragment.
 */
public class memoriaInterna extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    Uri URL;
    EditText caja_apellido, caja_nombre,caja_nombreArtistico;
    TextView datos;
    Button btn_guardar, btn_buscar;
    ImageButton btn_add_imagen;
    RecyclerView recyclerViewArtista;
    List<Artista> listarArtistas;
    artistaAdapter adapter;

    public memoriaInterna() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cargar();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment memoriaInterna.
     */
    // TODO: Rename and change types and number of parameters
    public static memoriaInterna newInstance(String param1, String param2) {
        memoriaInterna fragment = new memoriaInterna();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void cargar(){
        btn_guardar = getActivity().findViewById(R.id.btn_memoria_guardar);
        btn_buscar = getActivity().findViewById(R.id.btn_memoria_buscar);
        btn_add_imagen= getActivity().findViewById(R.id.btn_memoria_addimagen);
        btn_guardar.setOnClickListener(this);
        btn_buscar.setOnClickListener(this);
        btn_add_imagen.setOnClickListener(this);
        caja_nombreArtistico = getActivity().findViewById(R.id.lbl_memoria_nombreArtistico);
        caja_apellido= getActivity().findViewById(R.id.txt_memoria_apellido);
        caja_nombre= getActivity().findViewById(R.id.txt_memoria_nombre);
        datos = getActivity().findViewById(R.id.lbl_memoria_datos);
        recyclerViewArtista = getActivity().findViewById(R.id.containerViewer_artista);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_memoria_interna, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_memoria_guardar:
                try{
                    OutputStreamWriter escritor = new OutputStreamWriter(getActivity().openFileOutput("archivo2.txt", Context.MODE_APPEND));
                    escritor.write(caja_apellido.getText().toString()+","+caja_nombre.getText().toString()+","+caja_nombreArtistico.getText().toString()+","+URL.toString()+";");
                    escritor.close();
                }
                catch(Exception ex){
                    Log.e("archivoM2","Error de escritura:"+ex.getMessage() );
                }

                break;
            case  R.id.btn_memoria_buscar:
                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(getActivity().openFileInput("archivo2.txt")));
                    String lineas = reader.readLine();
                    cargarRecycler(lineas.split(";"),",");

                }

                catch(Exception ex){
                    Log.e("archivoM2","Error de lectura:"+ex.getMessage() );

                }
                break;
            case R.id.btn_memoria_addimagen:
                Intent intent;
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");

                startActivityForResult(intent.createChooser(intent,"Selecciona imaged"),200);

                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==getActivity().RESULT_OK) {
            URL = data != null ? data.getData() : null;
            btn_add_imagen.setImageURI(URL);

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void cargarRecycler(String[] vector,String separador){
        listarArtistas = new ArrayList<Artista>();
        String[] artistas ;
        for (int i = 0; i < vector.length ; i++) {
            artistas = vector[i].split(separador);
            Artista a = new Artista();
            for (int j = 0; j <artistas.length ; j++) {
                a.setNombres(artistas[0]);
                a.setApellidos(artistas[1]);
                a.setPseudo(artistas[2]);
                a.setFoto(artistas[3]);
            }
            listarArtistas.add(a);
        }


        adapter = new artistaAdapter(listarArtistas, new ProductoIn() {
            @Override
            public void onCLick(View v, int pos) {
                TextView nombre_comercial = v.findViewById(R.id.lbl_nombre_comercial_artista);
                ImageView dato_imagen = v.findViewById(R.id.imagen);
                TextView nombre = v.findViewById(R.id.lbl_nombre_artista);
                TextView apellido = v.findViewById(R.id.lbl_apellido_artista);

                final Dialog dlgartista = new Dialog(getContext());
                dlgartista.setContentView(R.layout.dlg_artista);
                final TextView caja1 = dlgartista.findViewById(R.id.lbl_dialog_artista);
                final TextView caja2 = dlgartista.findViewById(R.id.lbl_dialog_apellido);
                final TextView caja3 = dlgartista.findViewById(R.id.lbl_dialog_nombre);
                final ImageView imagenview = dlgartista.findViewById(R.id.imagen_dialog_artista);
                ImageButton btn = dlgartista.findViewById(R.id.btn_dialog_image);


                caja1.setText(nombre_comercial.getText());
                caja2.setText(nombre.getText());
                caja3.setText(apellido.getText());
                imagenview.setImageDrawable(dato_imagen.getDrawable());



                dlgartista.show();
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgartista.hide();

                    }
                });
            }

        });
        recyclerViewArtista.setLayoutManager(new LinearLayoutManager(getContext()));

        DividerItemDecoration divider = new DividerItemDecoration(recyclerViewArtista.getContext(),((LinearLayoutManager)recyclerViewArtista.getLayoutManager()).getOrientation());
        recyclerViewArtista.addItemDecoration(divider);
        recyclerViewArtista.setAdapter(adapter);
    }
}
