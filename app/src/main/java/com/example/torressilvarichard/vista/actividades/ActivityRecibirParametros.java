package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.torressilvarichard.Modelo.Producto;
import com.example.torressilvarichard.R;

import java.util.ArrayList;
import java.util.List;

public class ActivityRecibirParametros extends AppCompatActivity {

    TextView name, lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametros);

        cargarComponentes();
        cargarRecycler();
    }

    public void cargarComponentes(){
        String nombre, apellido;
        nombre = getIntent().getStringExtra("caja_nombre");
        apellido = getIntent().getStringExtra("apellido");
        name = findViewById(R.id.lblNombreRecibirParametro);
        lastName = findViewById(R.id.lblApellidoRecibirParametro);
        name.setText("El Nombre es: " +nombre);
        lastName.setText("El Apellido es: "+apellido);
    }

    public void cargarRecycler(){
        List<Producto> Li = new ArrayList<Producto>();


    }
}
