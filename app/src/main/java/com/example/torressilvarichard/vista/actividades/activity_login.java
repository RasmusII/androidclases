package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.torressilvarichard.R;

public class activity_login extends AppCompatActivity implements View.OnClickListener {

    EditText txtUser, txtPassword;
    Button boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        cargarComponentes();

    }

    private void cargarComponentes(){
        txtUser = findViewById(R.id.txtUsuario);
        txtPassword = findViewById(R.id.txtPass);
        boton = findViewById(R.id.btnLogin);
        boton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText( activity_login.this, "Usuario: " + txtUser.getText() + "clave: " + txtPassword.getText(), Toast.LENGTH_SHORT).show();
    }
}
