package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.torressilvarichard.Modelo.Artista;
import com.example.torressilvarichard.R;
import com.example.torressilvarichard.vista.adapter.artistaAdapter;

import java.util.ArrayList;
import java.util.List;

public class activityRecyclerartistas extends AppCompatActivity {

    RecyclerView recyclerViewArtista;
    artistaAdapter adapter;
    List<Artista> listaArtista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerartistas);
        tomarControl();
        cargarRecycler();
    }

    private void tomarControl(){
        recyclerViewArtista = findViewById(R.id.recyclerArtista);
    }

    private void cargarRecycler(){
        Artista artista1 = new Artista();
        artista1.setApellidos("JOVI");
        artista1.setNombres("BON");

        Artista artista2 = new Artista();
        artista2.setApellidos("Medardo");
        artista2.setNombres("Manuel");

        Artista artista3 = new Artista();
        artista3.setApellidos("XD");
        artista3.setNombres("KAPPA");

        listaArtista = new ArrayList<>();

        listaArtista.add(artista1);
        listaArtista.add(artista2);

        adapter = new artistaAdapter(listaArtista);
        recyclerViewArtista.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewArtista.setAdapter(adapter);
    }
}
