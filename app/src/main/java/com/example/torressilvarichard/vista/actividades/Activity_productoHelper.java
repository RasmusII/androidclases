package com.example.torressilvarichard.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.torressilvarichard.Modelo.Producto;
import com.example.torressilvarichard.R;

import com.example.torressilvarichard.controlador.HelperProducto;
import com.example.torressilvarichard.vista.adapter.HelperProductoAdapter;
import com.example.torressilvarichard.vista.adapter.ProductoIn;

import java.util.List;

public class Activity_productoHelper extends AppCompatActivity implements View.OnClickListener {

    EditText codigo, cantidad, precio, descripcion;
    RecyclerView listarProductos;
    Button agregar, buscar, buscarCo, eliminarAll, eliminarCod, modificar;

    HelperProductoAdapter adapter;

    List<Producto> pro;

    HelperProducto hp = new HelperProducto(this, "producto", null, 1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_helper);

        controlar();
    }

    public void controlar(){
        codigo = findViewById(R.id.txtCodigo);
        cantidad = findViewById(R.id.txtCantidad);
        precio = findViewById(R.id.txtCantidad);
        descripcion = findViewById(R.id.txtDespripcion);
        listarProductos = findViewById(R.id.listarProductos);
        agregar = findViewById(R.id.btnAgregar);
        buscar = findViewById(R.id.btnBuscar);
        buscarCo = findViewById(R.id.btnBuscarCodigo);
        eliminarAll = findViewById(R.id.btnEliminarAll);
        eliminarCod = findViewById(R.id.btnEliminarCodigo);
        modificar = findViewById(R.id.btnModificar);

        agregar.setOnClickListener(this);
        buscar.setOnClickListener(this);
        buscarCo.setOnClickListener(this);
        eliminarCod.setOnClickListener(this);
        eliminarAll.setOnClickListener(this);
        modificar.setOnClickListener(this);
    }

    public void Recycler(){
        pro = hp.getAll();
        adapter = new HelperProductoAdapter(pro, new ProductoIn() {
            @Override
            public void onCLick(View v, int pos) {
                TextView re_codigo, re_descripcion, re_precio, re_cantidad;

                re_codigo = v.findViewById(R.id.lblCodigo);
                re_descripcion = v.findViewById(R.id.lblDescripsion);


                codigo.setText(re_codigo.getText().toString());
                descripcion.setText(re_descripcion.getText().toString());
            }
        });
        listarProductos.setLayoutManager(new LinearLayoutManager(this));
        listarProductos.setAdapter(adapter);
    }


    Producto p;

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAgregar:
                p = new Producto();
                p.setCodigo(Integer.parseInt(codigo.getText().toString()));
                p.setDescripcion(descripcion.getText().toString());
                p.setPrecio(Double.parseDouble(precio.getText().toString()));
                p.setCantidad(Integer.parseInt(cantidad.getText().toString()));
                hp.insertar(p);
                break;
            case R.id.btnBuscar:
                Recycler();
                break;
            case R.id.btnModificar:
                p = new Producto();
                p.setCodigo(Integer.parseInt(codigo.getText().toString()));
                p.setDescripcion(descripcion.getText().toString());
                p.setPrecio(Double.parseDouble(precio.getText().toString()));
                p.setCantidad(Integer.parseInt(cantidad.getText().toString()));
                hp.modificar(p);
                break;
            case R.id.btnEliminarCodigo:
                hp.eliminar(codigo.getText().toString());
                break;
            case R.id.btnEliminarAll:
                hp.eliminarAll();
                break;
            case R.id.btnBuscarCodigo:
                pro = hp.buscarCodigo(codigo.getText().toString());
                adapter = new HelperProductoAdapter(pro);
                listarProductos.setLayoutManager(new LinearLayoutManager(this));
                listarProductos.setAdapter(adapter);
                break;
        }
    }
}
