package com.example.torressilvarichard.vista.fragmentos;

import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.FragmentManager;

import android.net.Uri;
import android.os.Bundle;

import com.example.torressilvarichard.Modelo.Comunicador;
import com.example.torressilvarichard.R;


public class Activity_escucharFragmento extends AppCompatActivity implements Comunicador, Frag1.OnFragmentInteractionListener, Frag2.OnFragmentInteractionListener {

    static int clicks=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escuchar_fragmento);
        cargar();
    }

    @Override
    public void responder(String datos) {
        FragmentManager FM = getSupportFragmentManager();
        Frag2 fragmentoRecibir = (Frag2) FM.findFragmentById(R.id.fragment8);
        fragmentoRecibir.cambiarTexto(datos);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void cargar(){

    }



}
