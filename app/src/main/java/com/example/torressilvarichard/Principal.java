package com.example.torressilvarichard;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.torressilvarichard.controlador.ServicioWebVolly;
import com.example.torressilvarichard.vista.ActivityMemoriaApp;
import com.example.torressilvarichard.vista.ActivityMemoryI;
import com.example.torressilvarichard.vista.actividades.ActividadCarroORM;
import com.example.torressilvarichard.vista.actividades.Activity_productoHelper;
import com.example.torressilvarichard.vista.actividades.Activity_volly;
import com.example.torressilvarichard.vista.actividades.ServicioWebClima;
import com.example.torressilvarichard.vista.actividades.actividad_SW;
import com.example.torressilvarichard.vista.actividades.activityArchivos;
import com.example.torressilvarichard.vista.actividades.activityRecyclerartistas;
import com.example.torressilvarichard.vista.actividades.activity_login;
import com.example.torressilvarichard.vista.actividades.activity_suma;
import com.example.torressilvarichard.vista.actividades.ActivityParametro;
import com.example.torressilvarichard.vista.fragmentos.Activity_escucharFragmento;
import com.example.torressilvarichard.vista.fragmentos.fragmento;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

public class Principal extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send, R.id.artistas, R.id.ORMS)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch(item.getItemId()){
            case R.id.opcion_login:
                intent = new Intent(this, activity_login.class);
                startActivity(intent);
                break;
            case R.id.opcion_sumar:
                intent = new Intent(this, activity_suma.class);
                startActivity(intent);
                break;
            case R.id.opcion_parametro:
                intent = new Intent(this, ActivityParametro.class);
                startActivity(intent);
                break;
            case R.id.colores:
                intent = new Intent(this, fragmento.class);
                startActivity(intent);
                break;
            case R.id.opcion_data_fragmentos:
                intent = new Intent(this, Activity_escucharFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionSumar:
                final Dialog dlg_sumar = new Dialog(Principal.this);
                dlg_sumar.setContentView(R.layout.dlg_sumar);
                final EditText caja1 = dlg_sumar.findViewById(R.id.txtNUM1);
                final EditText caja2 = dlg_sumar.findViewById(R.id.txtNUM2);
                Button botonSuma = dlg_sumar.findViewById(R.id.btnSU);

                botonSuma.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int resul = Integer.parseInt(caja1.getText().toString()) + Integer.parseInt(caja2.getText().toString());
                        Toast.makeText(Principal.this, "suma es " + resul, Toast.LENGTH_SHORT).show();
                        dlg_sumar.hide();
                    }
                });

                dlg_sumar.show();

                break;
            case R.id.artistas:
                intent = new Intent(this, activityArchivos.class);
                startActivity(intent);
                break;
            case R.id.produc:
                intent = new Intent(Principal.this, Activity_productoHelper.class);
                startActivity(intent);
                break;
            case R.id.vehiculoOrm:
                intent = new Intent(this, ActividadCarroORM.class);
                startActivity(intent);
                break;
            case R.id.OpcionHilo:
                intent = new Intent(this, actividad_SW.class);
                startActivity(intent);
                break;
            case R.id.ServicioClima:
                intent = new Intent(this, ServicioWebClima.class);
                startActivity(intent);
                break;
            case  R.id.ServicioVoly:
                intent = new Intent(this, Activity_volly.class);
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
