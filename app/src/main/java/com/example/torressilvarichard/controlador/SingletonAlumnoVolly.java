package com.example.torressilvarichard.controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class SingletonAlumnoVolly {

    private RequestQueue queue;
    private Context context;
    private static SingletonAlumnoVolly myInstance;

    public SingletonAlumnoVolly(Context context){
        this.context = context;

        queue = getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if(queue == null){
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return queue;
    }

    public static synchronized SingletonAlumnoVolly getInstance(Context context){
        if (myInstance == null){
            myInstance = new SingletonAlumnoVolly(context);
        }
        return myInstance;
    }

    public <T> void addToRequestQueue(Request request){
        queue.add(request);
    }
}