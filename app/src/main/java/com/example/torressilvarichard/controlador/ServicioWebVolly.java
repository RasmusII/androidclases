package com.example.torressilvarichard.controlador;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.torressilvarichard.Modelo.alumno;

import org.json.JSONException;
import org.json.JSONObject;

public class ServicioWebVolly {

    String host="http://reneguaman.000webhostapp.com";
    String insert ="/insertar_alumno.php";
    String get ="/obtener_alumnos.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";
    String getById="/obtener_alumno_por_id.php";

    String consulta;
    Context context;
    boolean estado ;

    public ServicioWebVolly(Context context){
        this.context = context;
    }

    public String findAllStudents()
    {
        String path = host.concat(get) ;
        //RequestFuture<String> future = new RequestFuture<String>();

        StringRequest request = new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context, response, Toast.LENGTH_LONG).show();
                consulta= response;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);

        return consulta;
    }

    public boolean InsertStudent(alumno alumno){
        String path= host.concat(insert);
        JSONObject json = new JSONObject();

        try {
            json.put("nombre",alumno.getNombre());
            json.put("direccion",alumno.getDireccion());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        estado = true;
                        try {
                            Log.e("response",response.toString());
                            Toast.makeText(context,response.getString("estado"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);

        return estado;
    }

    public void delete(String s){
        String path = host.concat(delete);
        JSONObject json = new JSONObject();

        try {
            json.put("idalumno", s);
        }catch (JSONException e){
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,path,json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                estado = true;
                try {
                    Log.e("response", response.toString());
                    Toast.makeText(context, response.getString("estado"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse (VolleyError error){
                error.printStackTrace();
                }
            });

        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }


    public void update(alumno alumno){
        String path = host.concat(update);
        JSONObject json = new JSONObject();
        try {
            json.put("idalumno",alumno.getId());
            json.put("nombre",alumno.getNombre());
            json.put("direccion",alumno.getDireccion());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,path,json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                estado = true;
                try {
                    Log.e("response", response.toString());
                    Toast.makeText(context, response.getString("estado"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse (VolleyError error){
                error.printStackTrace();
            }
        });

        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
    }

    public String buscarId(String id){
        String path = host.concat(getById).concat("?idalumno=").concat(id);
        StringRequest request = new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
                consulta = response;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error:",error.getMessage());
            }
        });
        SingletonAlumnoVolly.getInstance(context).addToRequestQueue(request);
        return consulta;
    }

}
