package com.example.torressilvarichard.controlador;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.torressilvarichard.Modelo.Producto;

import java.util.ArrayList;
import java.util.List;

public class HelperProducto extends SQLiteOpenHelper {

    public HelperProducto(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    // creación de tablas
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table producto (id Integer primary key autoincrement, codigo Integer(5), descripcion text(50), precio double(5,2), cantidad Integer(5))");
    }

    // modificación de tablas
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertar(Producto producto){
        ContentValues valores = new ContentValues();

        // traer los valores
        valores.put("codigo",producto.getCodigo());
        valores.put("descripcion", producto.getDescripcion());
        valores.put("precio", producto.getPrecio());
        valores.put("cantidad", producto.getCantidad());

        // insertar los valores en la base de Datos
        this.getWritableDatabase().insert("producto",null,valores);

    }

    public List<Producto> getAll(){
        List<Producto> lista = new ArrayList<>();

        Cursor cursor = this.getReadableDatabase().rawQuery("Select * From producto ", null);

        if(cursor.moveToFirst()){
            do{
                Producto producto = new Producto();

                // obtener cada valor desde el cursor
                producto.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
                producto.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
                producto.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
                producto.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));

                lista.add(producto);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return lista;
    }


    public void modificar(Producto pro){

        ContentValues valores = new ContentValues();

        valores.put("codigo", pro.getCodigo());
        valores.put("descripcion", pro.getDescripcion());
        valores.put("precio", pro.getPrecio());
        valores.put("cantidad", pro.getCantidad());

        this.getWritableDatabase().update("producto", valores, "codigo = "+pro.getCodigo(), null);

    }

    public void eliminar(String code){
        this.getWritableDatabase().delete("producto", "codigo = '"+code+"'", null);
    }

    public void eliminarAll(){
        this.getWritableDatabase().delete("producto", null, null);
    }

    public List<Producto> buscarCodigo(String codigo){
        List<Producto> aux = new ArrayList<Producto>();

        Cursor cursor = this.getReadableDatabase().rawQuery("Select * from producto where codigo = '"+codigo+"'" , null);

        if(cursor.moveToFirst()){
            do{
                Producto producto = new Producto();

                // obtener cada valor desde el cursor
                producto.setCodigo(cursor.getInt(cursor.getColumnIndex("codigo")));
                producto.setDescripcion(cursor.getString(cursor.getColumnIndex("descripcion")));
                producto.setCantidad(cursor.getInt(cursor.getColumnIndex("cantidad")));
                producto.setPrecio(cursor.getDouble(cursor.getColumnIndex("precio")));

                aux.add(producto);
            }while (cursor.moveToNext());
        }
        cursor.close();

        return aux;
    }

}
