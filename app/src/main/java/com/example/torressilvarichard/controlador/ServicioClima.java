package com.example.torressilvarichard.controlador;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class ServicioClima extends AsyncTask<String,Void,String>{

    Context context;

    private AsyncInterface async;
    HashMap<String, Object> lista;

    public ServicioClima(Context context, AsyncInterface async){
        this.context = context;
        this.async = async;
    }

    @Override
    public String doInBackground(String... strings) {
        String consulta = "";
        URL url = null;
        String ruta = strings[0]; // esta es la ruta de obtener alumanos.php
        try {
            if (strings[1].equals("1")) {

                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                //Log.e("da",""+codigoRespuesta);
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader bf = new BufferedReader(new InputStreamReader(in));
                    consulta += bf.readLine();

                }
            }
        } catch (Exception ex) {
            Log.e("Exception", ex.toString());
        }
        return  consulta;
    }

    @Override
    public void onPostExecute(String s) {
        Log.e("data:",s);
        if (s != "") {
            Leer(s);
        }


    }

    public void Leer(String data){
        try {
            lista= new HashMap<>();
            JSONObject object = new JSONObject(data);
            JSONObject coord = object.getJSONObject("coord");
            lista.put("lon",coord.getDouble("lon"));
            lista.put("lat",coord.getDouble("lat"));
            JSONArray weather = object.getJSONArray("weather");

            lista.put("weather_main",weather.getJSONObject(0).getString("main"));
            lista.put("weather_description",weather.getJSONObject(0).getString("description"));
            lista.put("base",object.getString("base"));
            JSONObject main = object.getJSONObject("main");
            lista.put("temp",main.getDouble("temp"));
            lista.put("pressure",main.getDouble("pressure"));
            lista.put("humidity",main.getInt("humidity"));
            lista.put("temp_min",main.getDouble("temp_min"));
            lista.put("temp_max",main.getDouble("temp_max"));
            JSONObject wind = object.getJSONObject("wind");
            lista.put("speed",wind.getDouble("speed"));
            lista.put("deg",wind.getInt("deg"));
            lista.put("name",object.getString("name"));
            JSONObject sys = object.getJSONObject("sys");
            lista.put("message",sys.getDouble("message"));
            lista.put("country",sys.getString("country"));
            lista.put("sunrise",sys.getInt("sunrise"));
            lista.put("sunset",sys.getInt("sunset"));

            // si mo se sobrescribe el onpostexecute se puede usar el .get() y obtiene una cadena
            async.respuesta(lista);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
