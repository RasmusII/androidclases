package com.example.torressilvarichard.Modelo.config;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;

public class aplicacionORM extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
}
