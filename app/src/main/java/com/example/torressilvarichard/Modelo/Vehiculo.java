package com.example.torressilvarichard.Modelo;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.List;

@Table(name = "carro")
public class Vehiculo extends Model {

    @Column(name = "placa", unique = true)
    private String placa;

    @Column(name = "modelo", notNull = true)
    private String modelo;

    @Column(name = "marca", notNull = true)
    private String marca;

    @Column(name = "year", notNull = true)
    private int year;

    public Vehiculo(){
    }

    public Vehiculo(String placa, String modelo, String marca, int year) {
        super();
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.year = year;
    }



    public String getPlaca() {
        return placa;
    }

    public void setMlaca(String mlaca) {
        this.placa = mlaca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public static List<Vehiculo> getAllCarro(){
        return new Select().from(Vehiculo.class).execute();
    }

    public static Vehiculo getCarroPlate(String placa){
        return new Select().from(Vehiculo.class).where("placa = ?", placa).executeSingle();
    }

    public static List<Vehiculo> deleteAll(){
        return new Delete().from(Vehiculo.class).execute();
    }

    public static boolean eliminarPlaca(String placa){
        getCarroPlate(placa).delete();
        return true;
    }

    public static boolean modificar(Vehiculo carro){
        new Update(Vehiculo.class).set("placa=?,modelo=?,year=?,marca=?",carro.getPlaca(),carro.getModelo(),carro.getYear(), carro.getMarca()).where("placa=?",carro.getPlaca()).execute();
        return true;
    }
}
